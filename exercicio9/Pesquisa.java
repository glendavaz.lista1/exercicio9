import java.util.ArrayList;

public class Pesquisa
{
    private Habitante[] habitantes;
    private ArrayList<Habitante> habitantesList;

    public Pesquisa()
    {
      habitantes = new Habitante[10];
      habitantesList = new ArrayList<>();
    }
    
    public ArrayList<Habitante> getHabitantesList() {
        return habitantesList;
    }
    
    public void addHabitante(Habitante habitante) {
        this.habitantesList.add(habitante);
    }
    
    public double retornaMaiorAltura() {
        double maiorAltura = 0;
        
        for(int i = 0; i < habitantesList.size(); i++) {
            Habitante habitante = habitantesList.get(i);
            
            if(habitante.getAltura() > maiorAltura) {
                maiorAltura = habitante.getAltura();
            }
        }
        
        return maiorAltura;
    }
    
    public double retornaMenorAltura() {
        double menorAltura = 0;
        
        for(int i = 0; i < habitantesList.size(); i++) {
            Habitante habitante = habitantesList.get(i);
            
            if((habitante.getAltura() > 0 && habitante.getAltura() < menorAltura ) || i == 0) {
                menorAltura = habitante.getAltura();
            }
                
        }
        return menorAltura;
    }
    
    public double retornaMediaAlturaMulheres() {
        double somaAltura = 0;
        int quantidade = 0;
        for(int i = 0; i < habitantesList.size(); i++) {
           Habitante habitante = habitantesList.get(i);
           if(habitante.getSexo() == 1) {
               quantidade++;
               somaAltura = somaAltura + habitante.getAltura();    
           }
        }
        
        double mediaAltura = somaAltura / quantidade;
        
        return mediaAltura;
    }
    
    public int retornaNumeroHomens() {
        int quantidade = 0;
        for(int i = 0; i < habitantesList.size(); i++) {
           Habitante habitante = habitantesList.get(i);
           if(habitante.getSexo() == 0) {
               quantidade++;                 
           }
        }

        return quantidade;
    }
    
    public double[] retornaPorcentagemHomensMulheres() {
        
        int quantidadeHomens = 0;
        for(int i = 0; i < habitantesList.size(); i++) {
           Habitante habitante = habitantesList.get(i);
           if(habitante.getSexo() == 0) {
               quantidadeHomens++;                 
           }
        }
        
        double[] percentual = new double[2];
                      
        percentual[0] = (quantidadeHomens / habitantesList.size()) * 100;
        percentual[1] = 100 - percentual[0];
        
        
        return percentual;        
        
    }
    
    public double retornaPorcentagemMulheres() {
        int totalMulheres = 0;
        int totalRegras = 0;
        for(int i = 0; i < habitantesList.size(); i++) {
           Habitante habitante = habitantesList.get(i);
           if(habitante.getSexo() == 1) {
               totalMulheres++;
               if(habitante.getIdade() >= 18 && habitante.getIdade() <= 35 && habitante.getCorCabelos() == 1 && habitante.getCorOlhos() == 1) {
                   totalRegras++;
               }
           }
        }
        
        double percentual = (totalRegras / totalMulheres) * 100;
        
        return percentual;       
        
    }
  
  
}
