import java.util.Scanner;
public class Principal
{
    public static void main (String[] args) {
        Scanner le = new Scanner(System.in);
        
        Pesquisa pesquisa = new Pesquisa();
        
        while(true) {
            System.out.println("### Habitante");
            Habitante habitante = new Habitante();
            System.out.println("Insira o sexo do habitante (0 - Masculino e 1 - Feminino):");
            habitante.setSexo(le.nextInt());
            System.out.println("Insira a cor dos olhos do habitante (0 - Azuis | 1 - Verdes | 2 - Pretos | 3 - Castanhos):");
            habitante.setCorOlhos(le.nextInt());
            System.out.println("Insira a cor dos cabelos do habitante (0 - Preto | 1 - Loiro | 2 - Ruivo | 3 - Castanho):");
            habitante.setCorCabelos(le.nextInt());
            System.out.println("Insira a idade do habitante:");
            habitante.setIdade(le.nextInt());
            System.out.println("Insira a altura do habitante:");
            habitante.setAltura(le.nextDouble());
            
            pesquisa.addHabitante(habitante);
            
            System.out.println("Deseja cadastrar outro habitante? (S ou N)");
            if(le.next().equalsIgnoreCase("n")) {
                break;
            }
            
        }
        
        System.out.println("A maior altura e " + pesquisa.retornaMaiorAltura());
        System.out.println("A menor altura e " + pesquisa.retornaMenorAltura());
        System.out.println("A media de altura entre as mulheres e " + pesquisa.retornaMediaAlturaMulheres());
        System.out.println("O numero de habitantes homens e " + pesquisa.retornaNumeroHomens());        
        System.out.println("A porcentagem de homens e " + pesquisa.retornaPorcentagemHomensMulheres()[0] + "% e a porcentagem de mulheres e " + pesquisa.retornaPorcentagemHomensMulheres()[1] + "%");
        System.out.println("A porcentagem de indivíduos do sexo feminino cuja idade esteja entre 18 e 35 anos,inclusive, e que tenham olhos verdes e cabelos louros e " + pesquisa.retornaPorcentagemMulheres() + "%");
        
    }
  
}
