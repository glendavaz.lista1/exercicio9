public class Habitante
{
    private int sexo;
    private int corOlhos;
    private int corCabelos;
    private int idade;
    public double altura;


    public Habitante()
    {

    }
    
    public void setSexo(int paramSexo) {
        this.sexo = paramSexo;
    }
    
    public int getSexo() {
        return this.sexo;
    }
    
    public void setCorOlhos(int paramCorOlhos) {
        this.corOlhos = paramCorOlhos;
    }
    
    public int getCorOlhos() {
        return this.corOlhos;
    }
    
    public void setCorCabelos(int paramCorCabelos) {
        this.corCabelos = paramCorCabelos;
    }
    
    public int getCorCabelos() {
        return this.corCabelos;
    }
    
    public void setIdade(int paramIdade) {
        this.idade = paramIdade;
    }
    
    public int getIdade() {
        return this.idade;
    }
    
    public void setAltura(double paramAltura) {
        this.altura = paramAltura;
    }
    
    public double getAltura() {
        return this.altura;
    }

    
}
